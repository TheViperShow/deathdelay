package me.thevipershow.deathdelay;

import me.thevipershow.deathdelay.listeners.DeathListeners;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class DeathDelay extends JavaPlugin {

    @Override
    public void onEnable() { // Plugin startup logic
        DeathListeners deathListeners = new DeathListeners(this);
        Bukkit.getPluginManager().registerEvents(deathListeners, this);
    }
}
