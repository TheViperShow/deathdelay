package me.thevipershow.deathdelay.listeners;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

public final class DeathListeners implements Listener {

    private final Plugin plugin;
    private final static byte CHAT_ACTION_BAR = 0x2;
    private final static byte PLAYER_SLEEP_HEIGHT_FIX = 0xF / 0xA;
    private final static long DEATH_DELAY = 20L * 5L;
    private final static long TIMER_UPDATE = 5L;
    private final static int SERVER_VIEW_DISTANCE = Bukkit.getServer().getViewDistance();

    public DeathListeners(Plugin plugin) {
        this.plugin = plugin;
    }

    private final HashMap<UUID, Long> playerDelays = new HashMap<>();

    public static String generateRemainingTimeString(long timePassedTicks) {
        StringBuilder builder = new StringBuilder("§b[");
        byte start = 0x0;
        final long toColor = 0x14 * timePassedTicks / DEATH_DELAY;
        while (start <= 0x14) {
            builder.append('§')
                    .append(start > toColor ? 'c' : 'a')
                    .append('|');
            start++;
        }
        return builder.append("§b] §7§lRespawn in: §a§l")
                .append((String.format("%.2f", (DEATH_DELAY - timePassedTicks) / 20f)))
                .append("§7§ls").toString();
    }

    public static EntityPlayer createPlayer(Player player) {
        MinecraftServer minecraftServer = ((CraftServer) Bukkit.getServer()).getServer(); // get server
        WorldServer world = ((CraftWorld) player.getWorld()).getHandle(); // get the world the player who died was standing in.
        GameProfile botGameProfile = new GameProfile(UUID.randomUUID(), player.getName()); // create a gameProfile for the bot
        GameProfile playerProfile = ((CraftPlayer) player).getProfile(); // get the skin of the player who just died
        Collection<Property> skinProperty = playerProfile.getProperties().get("textures"); // get his textures
        botGameProfile.getProperties().putAll("textures", skinProperty); // put the textures on the bot
        PlayerInteractManager interactManager = new PlayerInteractManager(world);
        EntityPlayer botPlayer = new EntityPlayer(minecraftServer, world, botGameProfile, interactManager); // initialize bot

        Location playerLocation = player.getLocation();
        botPlayer.setLocation(playerLocation.getX(), // set his location on player death
                playerLocation.getY(),
                playerLocation.getZ(),
                playerLocation.getYaw(),
                playerLocation.getPitch());
        return botPlayer;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerDeath(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) return;
        Player player = (Player) event.getEntity();
        if (player.getHealth() - event.getDamage() > 0.0d) return;
        event.setCancelled(true);
        UUID uuid = player.getUniqueId();
        if (playerDelays.containsKey(uuid)) return;
        player.setGameMode(GameMode.SPECTATOR);
        playerDelays.put(uuid, 0L);
        Location deathLocation = player.getLocation();
        BlockPosition deathBlockPosition = new BlockPosition(deathLocation.getX(), deathLocation.getY(), deathLocation.getZ());

        EntityPlayer botPlayer = createPlayer(player);
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getLocation().distance(deathLocation) > SERVER_VIEW_DISTANCE) continue;
            PlayerConnection conn = ((CraftPlayer) p).getHandle().playerConnection;
            conn.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, botPlayer));
            conn.sendPacket(new PacketPlayOutNamedEntitySpawn(botPlayer)); // spawn the bot to nearby players.
            conn.sendPacket(new PacketPlayOutBed(botPlayer, deathBlockPosition)); // make the bot 'sleep'
            conn.sendPacket(new PacketPlayOutEntity.PacketPlayOutRelEntityMove(botPlayer.getId(), // fix the bot height
                    (byte) 0, // we do not need to change X or Z values.
                    PLAYER_SLEEP_HEIGHT_FIX, // decrease height by 0.15f
                    (byte) 0,
                    true));
        }

        BukkitTask task = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            if (player.isOnline()) {
                //PacketContainer actionBarPacket = new PacketContainer(PacketType.Play.Server.CHAT);
                //actionBarPacket.getChatComponents().write(0, WrappedChatComponent.fromText(generateRemainingTimeString(playerDelays.get(uuid))));
                //actionBarPacket.getBytes().write(0, CHAT_ACTION_BAR);
                //try {
                //    protocolManager.sendServerPacket(player, actionBarPacket);
                //} catch (InvocationTargetException e) {
                //    e.printStackTrace();
                //}

                PlayerConnection playerConnection = ((CraftPlayer) player).getHandle().playerConnection;
                IChatBaseComponent chatBaseComponent = new ChatMessage(generateRemainingTimeString(playerDelays.get(uuid)));
                PacketPlayOutChat chatPacket = new PacketPlayOutChat(chatBaseComponent, CHAT_ACTION_BAR);
                playerConnection.sendPacket(chatPacket);

                playerDelays.computeIfPresent(uuid, (k, v) -> v += TIMER_UPDATE);
            }

        }, 1L, TIMER_UPDATE);

        Bukkit.getScheduler().runTaskLater(plugin, () -> {
            task.cancel();
            playerDelays.remove(uuid);
            if (!player.isOnline()) return;
            player.setGameMode(GameMode.SURVIVAL);
            for (Player p : Bukkit.getOnlinePlayers()) {
                ((CraftPlayer) p).getHandle().playerConnection
                        .sendPacket(new PacketPlayOutEntityDestroy(botPlayer.getId()));
            }
        }, DEATH_DELAY);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        UUID uuid = player.getUniqueId();
        playerDelays.remove(uuid);
        player.setGameMode(GameMode.SURVIVAL);
    }
}
